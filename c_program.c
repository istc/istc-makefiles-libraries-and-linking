/* c_program.c */

#include <stdio.h>
#include <stdlib.h>

#include "c_function.h"
#include "c_function_d.h"
 
int main(int argc, char **argv){

   int val = strtol (argv[1], NULL, 10);
   printf("Running C program with val = %d\n", val);

   c_function(&val);
   printf("c_function returns with val = %d\n", val);

   double d_val = (double)val;
   c_function_d(&d_val);
   printf("c_function_d returns with d_val = %lf\n", d_val);
  
   printf("C program complete.\n\n");
   return 0;
}
