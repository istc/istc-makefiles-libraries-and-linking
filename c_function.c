 /* c_function.c */
    
#include "c_function.h"
 
/* multiply the input value by 2 */
void c_function(int *val){
   *val = *val * 2;
}

