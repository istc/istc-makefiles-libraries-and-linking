/* c_function_d.c */

#include "c_function_d.h"

/* multiply the input value by 2.0 */
void c_function_d(double *d_val){
   *d_val = *d_val * 2.0;
}

