#!/usr/bin/env make

default:	
	@echo "No target specified."

run: c_program
	@echo
	./c_program 1

clean: 
	rm -f *.so *.a *.o c_program 

c_function.o: c_function.c c_function.h
	gcc -c c_function.c

c_function_d.o: c_function_d.c c_function_d.h
	gcc -c c_function_d.c

c_program.o: c_program.c
	gcc -c c_program.c

c_program:  c_program.o c_function.o c_function_d.o 
	gcc c_program.o c_function.o c_function_d.o -o c_program

.PHONY: run clean

